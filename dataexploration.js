import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
PhoneData = new Mongo.Collection('phonedata');
// Plotly = require('plotly.js');

Router.route('/', {
	name: 'home',
	template: 'home'	
});

Router.route('/chartContainer');
Meteor.methods({
	'generateMedian': function(inputArray, sampleRate) {
		console.log("Median function entered");
		var sum = 0;
		var median = 0;
		var time = 0;
		var newArray = [];
		var iterationLimit = inputArray.length - sampleRate;
		var tempArray = []
		for(var i=0; i<iterationLimit; i+=sampleRate) {
			if(i>inputArray.length) {
				console.log("i is:",i);
				break;
			}
			tempArray = [];
			console.log("i is: ",i,"\n");
			for(var j=i; j<i+sampleRate; j++){
				tempArray.push(inputArray[j].y);
			}
			tempArray.sort();
			middle = (tempArray.length-1)/2;
			middle +=1;
			console.log("median is ", tempArray[middle]);
			median = tempArray[middle];
			console.log("Median is",median);
			var obj = {x:(inputArray[i].x),y:median}
			console.log("Obj is ",obj);
			newArray.push(obj);
		}
		console.log("New Array is ",newArray);
		return newArray;
	},
	'generateMean': function(inputArray, sampleRate) {
		console.log("Mean function entered");
		console.log(inputArray[0].x);
		console.log(inputArray[0].y);
		var sum = 0;
		var average = 0;
		var time = 0;
		var newArray = [];
		var sample = parseInt
		console.log("sampleRate is ",sampleRate);
		console.log("inputArray.length is ", inputArray.length);
		var iterationLimit = inputArray.length - sampleRate;
		for(var i=0; i<iterationLimit; i+=sampleRate) {
			if(i>inputArray.length) {
				console.log("i is:",i);
				break;
			}
			console.log("i is: ",i,"\n");
			sum = 0;
			average = 0;
			for(var j=i; j<i+sampleRate; j++){
				sum+=inputArray[j].y;
				console.log("sum is:",sum);
			}
			average = sum/sampleRate;
			console.log("Average is",average);
			var obj = {x:(inputArray[i].x),y:average}
			console.log("Obj is ",obj);
			newArray.push(obj);
		}
		console.log("New Array is ",newArray);
		return newArray;
	},
	'generateMaximum': function(inputArray, sampleRate) {
		console.log("Maximum function entered");
		var sum = 0;
		var maximum = 0;
		var time = 0;
		var newArray = [];
		var iterationLimit = inputArray.length - sampleRate;
		for(var i=0; i<iterationLimit; i+=sampleRate) {
			var tempArray = [];
			for(var j=i; j<i+sampleRate; j++){
				tempArray[j] = inputArray[j].y;
				console.log(tempArray[j]);
			}		
			tempArray.sort();
			maximum = tempArray[0];
			var obj = {x:(inputArray[i].x),y:maximum}
			console.log("Max is ",maximum);
			newArray.push(obj);
		}
		console.log("New Array is ",newArray);
		return newArray;
	},
	'generateMinimum': function(inputArray, sampleRate) {
		console.log("Minimum function entered");
		var sum = 0;
		var minimum = 0;
		var time = 0;
		var newArray = [];
		var iterationLimit = inputArray.length - sampleRate;
		for(var i=0; i<iterationLimit; i+=sampleRate) {
			var tempArray = [];
			for(var j=i; j<i+sampleRate; j++){
				tempArray[j] = inputArray[j].y;
				console.log(tempArray[j]);
			}		
			tempArray.sort();
			minimum = tempArray[sampleRate-1];
			var obj = {x:(inputArray[i].x),y:minimum}
			console.log("Min is ",minimum);
			newArray.push(obj);
		}
		console.log("New Array is ",newArray);
		return newArray;
	},
	'generateStandardDeviation': function(inputArray,sampleRate) {
		var sum = 0;
		var diff = 0;
		var average = 0;
		var time = 0;
		var newArray = [];
		var diffSum = 0;
		var iterationLimit = inputArray.length - sampleRate;
		for(var i=0; i<iterationLimit; i+=sampleRate) {
			if(i>inputArray.length) {
				console.log("i is:",i);
				break;
			}
			console.log("i is: ",i,"\n");
			sum = 0;
			average = 0;
			diffSum = 0;
			for(var j=i; j<i+sampleRate; j++){
				sum+=inputArray[j].y;
				console.log("sum is:",sum);
			}
			average = sum/sampleRate;
			for(var j=i; j<i+sampleRate; j++){
				diff = inputArray[j].y - average;
				diff = diff*diff;
				diffSum += diff;
			}
			diffAverage = diffSum/sampleRate;
			console.log("Average is",diffAverage);
			var obj = {x:(inputArray[i].x),y:diffAverage}
			console.log("Obj is ",obj);
			newArray.push(obj);

		}
		console.log("New Array is ",newArray);
		return newArray;
	},
	'generateIQR': function(inputArray, sampleRate) {
		var newArray = [];
		var iterationLimit = inputArray.length - sampleRate;
		var square = 0;
		var sum = 0;
		var average = 0;
		console.log("SampleRate is",sampleRate/4);
		var quarter = sampleRate/4;
		var thirdQuartile = quarter*3;
		quarter = parseInt(quarter);
		thirdQuartile = parseInt(thirdQuartile);
		console.log("Quarter:",quarter,"thirdQuartile",thirdQuartile);	
		var quarterValue = 0;
		var thirdQuartileValue = 0
		for(var i=0; i<iterationLimit; i++) {
			var tempArray = [];
			for(var j=i; j<i+sampleRate; j++) {
				tempArray[j] = inputArray[j].y;
			}
			tempArray.sort();
			thirdQuartileValue = 0;
			if(quarter%2!=0) {
				quarterValue = tempArray[quarter-1] + tempArray[quarter];
				quarterValue = quarterValue/2;
				console.log("QuarterValue is",quarterValue);
			}
			else {
				quarterValue = tempArray[quarter];
				console.log("QuarterValue is",quarterValue);
			}
			if(thirdQuartile%2!=0) {
				thirdQuartileValue = tempArray[thirdQuartile-1] + tempArray[thirdQuartile];
				thirdQuartileValue = thirdQuartileValue/2;
				console.log("thirdQuartileValue is",thirdQuartileValue);
			}
			else {
				thirdQuartileValue = tempArray[thirdQuartile];
			}
			console.log("QuarteValue:",quarterValue,"thirdQuartileValue",thirdQuartileValue);
			average = thirdQuartileValue - quarterValue;
			console.log("average is ",average);
			var obj = {x:(inputArray[i].x),y:average}
			newArray.push(obj);
		}
		return newArray;
	},
	'generateRMS': function(inputArray, sampleRate) {
		var newArray = [];
		var iterationLimit = inputArray.length - sampleRate;
		var square = 0;
		var sum = 0;
		var average = 0;
		for(var i=0; i<iterationLimit; i++) {
			for(var j=i; j<i+sampleRate; j++) {
				sqaure = inputArray[j].y;
				square = square*square;
				console.log("Square is",square);
				sum += square;
			}
			console.log("sum is ",sum);
			average = sum/sampleRate;
			console.log("average is ",average);
			var obj = {x:(inputArray[i].x),y:average}
			newArray.push(obj);
		}
		return newArray;
	},
	'generate3AxisGraph': function(xArray, yArray, zArray) {
		var parameter = Session.get("selectedParameter");
		var max = Math.max.apply(Math,xArray);
		max-=2.0;
		var chart = new CanvasJS.Chart("chartContainer",
		{
		 animationEnabled: true,
		 zoomEnabled: true,
		 title:{
		   text: parameter
		 },
		 data: [
		 {
		 	name: "Sitting",
		   	type: "area",
		   	// color: "blue",
		   	xValueType: "dateTime",
		   	intervalType: "second",
		   	dataPoints: [{x:1421125364455.36987305,y:1.0, label: "Sitting"},
		   	{x:1421125521759.7902832,y:1.0, label: "Standing"}
		   	]
		   },
		   {
		 	name: "Standing",
		   	type: "area",
		   	// color: "orange",
		   	xValueType: "dateTime",
		   	intervalType: "second",
		   	dataPoints: [{x:1421125521759.7902832,y:1.0, label: "Standing"},
		   		{x:1421125757563.48022461,y:1.0, label: "Running"}
		   	]
		   },
		   {
		 	name: "Running",
		   	type: "area",
		   	// color: "red",
		   	xValueType: "dateTime",
		   	intervalType: "second",
		   	dataPoints: [{x:1421125757563.48022461,y:1.0, label: "Running"},
		   		{x:1421126046652.87988281,y:1.0, label: "Running"}
		   	]
		   },

		   {
		   	name:"X-axis",
		   	labelFontSize: 1,
		 	labelMaxWidth: 4,
		 	xValueType: "dateTime",
		 	intervalType: "second",
		 	type: "line",
		 	showInLegend: true,
		 	dataPoints: xArray
		 },
		 {
		 	name:"Y-axis",
		 	type: "line",
		 	xValueType: "dateTime",
		 	intervalType: "second",
		 	showInLegend: true,
		 	dataPoints: yArray
		 },
		 {
		 	name:"Z-axis",
		 	type: "line",
		 	xValueType: "dateTime",
		 	intervalType: "second",
		 	showInLegend: true,        
		 	dataPoints: zArray
		 }
		   
		 ],
		 legend: {
		   cursor: "pointer",
		   itemclick: function (e) {
		     if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		       e.dataSeries.visible = false;
		     } else {
		       e.dataSeries.visible = true;
		   }
		   chart.render();
		   }
		 }
	});
	chart.render();

	},
	'generateAccelerometerGraph'(){
		var query = {"sensor_infos.aa_x":1,"sensor_infos.aa_y":1,"sensor_infos.aa_z":1,"sensor_infos.t":1};
		var phoneArray = PhoneData.find(query).fetch();
		console.log(phoneArray);
		var count = phoneArray[0].sensor_infos.length;
		console.log("Parameter is: ",Session.get("selectedParameter"));
		console.log("Sample Rate is: ",Session.get("sampleRate"));
		console.log("Derived Function is: ",Session.get("function"));

		var count = phoneArray[0].sensor_infos.length;
		var xArray = [];
		for(var i=0; i<count; i++) {
			var xObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].aa_x};
			xArray.push(xObj);
		}

		var yArray = [];
		for(var i=0; i<count; i++) {
			var yObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].aa_y};
			yArray.push(yObj);
		}


		var zArray = [];
		for(var i=0; i<count; i++) {
			var zObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].aa_z};
			zArray.push(zObj);
		}
		var newXArray = [];
		var newYArray = [];
		var newZArray = [];
		console.log("")
		if(Session.get("function")!="") {
			console.log("Derived function if statement evaluated as true");
			console.log("Derived function is:",Session.get("function"));
			var derivedFunction = Session.get("function");
			var sampleRate = Session.get("sampleRate");
			newXArray = Meteor.call(derivedFunction, xArray, sampleRate);
			newYArray = Meteor.call(derivedFunction, yArray, sampleRate);
			newZArray = Meteor.call(derivedFunction, zArray, sampleRate);
		}
		else {
			newXArray = xArray;
			newYArray = yArray;
			newZArray = zArray;
		}
		Meteor.call('generate3AxisGraph',newXArray,newYArray,newZArray);
	},
	'generateMagnetometerGraph'(){
		console.log("Generate Magnetometer Graph function entered");
		var phoneArray = PhoneData.find({}).fetch();
		console.log(PhoneData);
		
		console.log(phoneArray);
		// var phoneArray = PhoneData.find().fetch();
		var count = phoneArray[0].sensor_infos.length;
		var xArray = [];
		for(var i=0; i<count; i++) {
			var xObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].mm_x};
			xArray.push(xObj);
		}

		var yArray = [];
		for(var i=0; i<count; i++) {
			var yObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].mm_y};
			yArray.push(yObj);
		}


		var zArray = [];
		for(var i=0; i<count; i++) {
			var zObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].mm_z};
			zArray.push(zObj);
		}
		var newXArray = [];
		var newYArray = [];
		var newZArray = [];
		console.log("")
		if(Session.get("function")!="") {
			console.log("Derived function if statement evaluated as true");
			console.log("Derived function is:",Session.get("function"));
			var derivedFunction = Session.get("function");
			var sampleRate = Session.get("sampleRate");
			newXArray = Meteor.call(derivedFunction, xArray, sampleRate);
			newYArray = Meteor.call(derivedFunction, yArray, sampleRate);
			newZArray = Meteor.call(derivedFunction, zArray, sampleRate);
		}
		else {
			newXArray = xArray;
			newYArray = yArray;
			newZArray = zArray;
		}
		Meteor.call('generate3AxisGraph',newXArray,newYArray,newZArray);
	},
	'generateMotionGravityDetectionGraph'(){
		var phoneArray = PhoneData.find().fetch();
		var count = phoneArray[0].sensor_infos.length;
		var xArray = [];
		for(var i=0; i<count; i++) {
			var xObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].mg_x};
			xArray.push(xObj);
		}
		var yArray = [];
		for(var i=0; i<count; i++) {
			var yObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].mg_y};
			yArray.push(yObj);
		}
		var zArray = [];
		for(var i=0; i<count; i++) {
			var zObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].mg_z};
			zArray.push(zObj);
		}
		var newXArray = [];
		var newYArray = [];
		var newZArray = [];
		console.log("")
		if(Session.get("function")!="") {
			console.log("Derived function if statement evaluated as true");
			console.log("Derived function is:",Session.get("function"));
			var derivedFunction = Session.get("function");
			var sampleRate = Session.get("sampleRate");
			newXArray = Meteor.call(derivedFunction, xArray, sampleRate);
			newYArray = Meteor.call(derivedFunction, yArray, sampleRate);
			newZArray = Meteor.call(derivedFunction, zArray, sampleRate);
		}
		else {
			newXArray = xArray;
			newYArray = yArray;
			newZArray = zArray;
		}
		Meteor.call('generate3AxisGraph',newXArray,newYArray,newZArray);
	},
	'generateMotionUserAccelerationGraph'(){
		var phoneArray = PhoneData.find().fetch();
		var count = phoneArray[0].sensor_infos.length;
		var xArray = [];
		for(var i=0; i<count; i++) {
			var xObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].mua_x};
			xArray.push(xObj);
		}
		var yArray = [];
		for(var i=0; i<count; i++) {
			var yObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].mua_y};
			yArray.push(yObj);
		}
		var zArray = [];
		for(var i=0; i<count; i++) {
			var zObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].mua_z};
			zArray.push(zObj);
		}
		var newXArray = [];
		var newYArray = [];
		var newZArray = [];
		console.log("")
		if(Session.get("function")!="") {
			console.log("Derived function if statement evaluated as true");
			console.log("Derived function is:",Session.get("function"));
			var derivedFunction = Session.get("function");
			var sampleRate = Session.get("sampleRate");
			newXArray = Meteor.call(derivedFunction, xArray, sampleRate);
			newYArray = Meteor.call(derivedFunction, yArray, sampleRate);
			newZArray = Meteor.call(derivedFunction, zArray, sampleRate);
		}
		else {
			newXArray = xArray;
			newYArray = yArray;
			newZArray = zArray;
		}
		Meteor.call('generate3AxisGraph',newXArray,newYArray,newZArray);
	},
	'generateMotionAttitudeGraph'(){
		var phoneArray = PhoneData.find().fetch();
		var count = phoneArray[0].sensor_infos.length;
		var xArray = [];
		for(var i=0; i<count; i++) {
			var xObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].ma_r};
			xArray.push(xObj);
		}

		var yArray = [];
		for(var i=0; i<count; i++) {
			var yObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].ma_y};
			yArray.push(yObj);
		}
		var newXArray = [];
		var newYArray = [];
		console.log("")
		if(Session.get("function")!="") {
			console.log("Derived function if statement evaluated as true");
			console.log("Derived function is:",Session.get("function"));
			var derivedFunction = Session.get("function");
			var sampleRate = Session.get("sampleRate");
			newXArray = Meteor.call(derivedFunction, xArray, sampleRate);
			newYArray = Meteor.call(derivedFunction, yArray, sampleRate);
		}
		else {
			newXArray = xArray;
			newYArray = yArray;
		}
		var max = Math.max.apply(Math, xArray);
		var chart = new CanvasJS.Chart("chartContainer",
		{
		 animationEnabled: true,
		 zoomEnabled: true,
		 title:{
		   text: "Motion Attitude Data"
		 },
		 data: [
		 {
		 	name: "Labels",
		   	type: "area",
		   	color: "blue",
		   	xValueType: "dateTime",
		   	intervalType: "second",
		   	dataPoints: [{x:1421125364455.36987305,y:max, label: "Sitting"},
		   	{x:1421125521759.7902832,y:max, label: "Standing"}
		   	]
		   },
		   {
		 	name: "Labels",
		   	type: "area",
		   	color: "orange",
		   	xValueType: "dateTime",
		   	intervalType: "second",
		   	dataPoints: [{x:1421125521759.7902832,y:max, label: "Standing"},
		   		{x:1421125757563.48022461,y:max, label: "Running"}
		   	]
		   },
		   {
		 	name: "Labels",
		   	type: "area",
		   	color: "red",
		   	xValueType: "dateTime",
		   	intervalType: "second",
		   	dataPoints: [{x:1421125757563.48022461,y:max, label: "Running"},
		   		{x:1421126046652.87988281,y:max, label: "Running"}
		   	]
		   },
		 {
		   name:"X-axis",
		   labelFontSize: 1,
		   labelMaxWidth: 4,
		   type: "line",
		   xValueType: "dateTime",
		   intervalType: "second",
		   showInLegend: true,
		   dataPoints: newXArray
		   },
		   {
		   name:"Y-axis",
		   type: "line",
		   xValueType: "dateTime",
		   intervalType: "second",
		   showInLegend: true,        
		   dataPoints: newYArray
		   }
		 ],
		 legend: {
		   cursor: "pointer",
		   itemclick: function (e) {
		     if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		       e.dataSeries.visible = false;
		     } else {
		       e.dataSeries.visible = true;
		   }
		   chart.render();
		   }
		 }
	});
	chart.render();
	},
	'generateGyroscopeRotationGraph'(){
		var phoneArray = PhoneData.find().fetch();
		var count = phoneArray[0].sensor_infos.length;
		var xArray = [];
		for(var i=0; i<count; i++) {
			var xObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].grr_x};
			xArray.push(xObj);
		}

		var yArray = [];
		for(var i=0; i<count; i++) {
			var yObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].grr_y};
			yArray.push(yObj);
		}


		var zArray = [];
		for(var i=0; i<count; i++) {
			var zObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].grr_z};
			zArray.push(zObj);
		}
		var newXArray = [];
		var newYArray = [];
		var newZArray = [];
		console.log("")
		if(Session.get("function")!="") {
			console.log("Derived function if statement evaluated as true");
			console.log("Derived function is:",Session.get("function"));
			var derivedFunction = Session.get("function");
			var sampleRate = Session.get("sampleRate");
			newXArray = Meteor.call(derivedFunction, xArray, sampleRate);
			newYArray = Meteor.call(derivedFunction, yArray, sampleRate);
			newZArray = Meteor.call(derivedFunction, zArray, sampleRate);
		}
		else {
			newXArray = xArray;
			newYArray = yArray;
			newZArray = zArray;
		}
		Meteor.call('generate3AxisGraph',newXArray,newYArray,newZArray);
	},
	'generateMotionUserAccelerationGraph'(){
		var phoneArray = PhoneData.find().fetch();
		var count = phoneArray[0].sensor_infos.length;
		var xArray = [];
		for(var i=0; i<count; i++) {
			var xObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].mua_x};
			xArray.push(xObj);
		}

		var yArray = [];
		for(var i=0; i<count; i++) {
			var yObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].mua_y};
			yArray.push(yObj);
		}


		var zArray = [];
		for(var i=0; i<count; i++) {
			var zObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].mua_z};
			zArray.push(zObj);
		}
		var newXArray = [];
		var newYArray = [];
		var newZArray = [];
		console.log("")
		if(Session.get("function")!="") {
			console.log("Derived function if statement evaluated as true");
			console.log("Derived function is:",Session.get("function"));
			var derivedFunction = Session.get("function");
			var sampleRate = Session.get("sampleRate");
			newXArray = Meteor.call(derivedFunction, xArray, sampleRate);
			newYArray = Meteor.call(derivedFunction, yArray, sampleRate);
			newZArray = Meteor.call(derivedFunction, zArray, sampleRate);
		}
		else {
			newXArray = xArray;
			newYArray = yArray;
			newZArray = zArray;
		}
		Meteor.call('generate3AxisGraph',newXArray,newYArray,newZArray);
	},
	'generateMotionRotationGraph'(){
		var phoneArray = PhoneData.find().fetch();
		var count = phoneArray[0].sensor_infos.length;
		var xArray = [];
		for(var i=0; i<count; i++) {
			var xObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].mrr_x};
			xArray.push(xObj);
		}

		var yArray = [];
		for(var i=0; i<count; i++) {
			var yObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].mrr_y};
			yArray.push(yObj);
		}


		var zArray = [];
		for(var i=0; i<count; i++) {
			var zObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].mrr_z};
			zArray.push(zObj);
		}
		var newXArray = [];
		var newYArray = [];
		var newZArray = [];
		console.log("")
		if(Session.get("function")!="") {
			console.log("Derived function if statement evaluated as true");
			console.log("Derived function is:",Session.get("function"));
			var derivedFunction = Session.get("function");
			var sampleRate = Session.get("sampleRate");
			newXArray = Meteor.call(derivedFunction, xArray, sampleRate);
			newYArray = Meteor.call(derivedFunction, yArray, sampleRate);
			newZArray = Meteor.call(derivedFunction, zArray, sampleRate);
		}
		else {
			newXArray = xArray;
			newYArray = yArray;
			newZArray = zArray;
		}
		Meteor.call('generate3AxisGraph',newXArray,newYArray,newZArray);
	},
	'generateMagneticHeadingGraph'() {

	},
	'generateTester'(){
		TESTER = document.getElementById('tester');

		Plotly.plot( TESTER, [{
    	x: [1, 2, 3, 4, 5],
    	y: [1, 2, 4, 8, 16] }], { 
    	margin: { t: 0 } } );

		/* Current Plotly.js version */
		console.log( Plotly.BUILD );
	},
	'generateCalibratedMagneticFieldGraph'() {
		var phoneArray = PhoneData.find().fetch();
		var count = phoneArray[0].sensor_infos.length;
		var xArray = [];
		for(var i=0; i<count; i++) {
			var xObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].cmf_x};
			xArray.push(xObj);
		}

		var yArray = [];
		for(var i=0; i<count; i++) {
			var yObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].cmf_y};
			yArray.push(yObj);
		}


		var zArray = [];
		for(var i=0; i<count; i++) {
			var zObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].cmf_z};
			zArray.push(zObj);
		}
		var newXArray = [];
		var newYArray = [];
		var newZArray = [];
		console.log("")
		if(Session.get("function")!="") {
			console.log("Derived function if statement evaluated as true");
			console.log("Derived function is:",Session.get("function"));
			var derivedFunction = Session.get("function");
			var sampleRate = Session.get("sampleRate");
			newXArray = Meteor.call(derivedFunction, xArray, sampleRate);
			newYArray = Meteor.call(derivedFunction, yArray, sampleRate);
			newZArray = Meteor.call(derivedFunction, zArray, sampleRate);
		}
		else {
			newXArray = xArray;
			newYArray = yArray;
			newZArray = zArray;
		}
		Meteor.call('generate3AxisGraph',newXArray,newYArray,newZArray);
	},
	'generateHeatMap'() {
		var phoneArray = PhoneData.find().fetch();
		var count = phoneArray[0].sensor_infos.length;
		var markers = [];
		var midPoint = parseInt(count/2);
		var latLngArray = [];
		for(var i=0; i<count; i++) {
			var lat = phoneArray[0].sensor_infos[i].lat;
			var lng = phoneArray[0].sensor_infos[i].lon;
			var latlngObj = {lat:lat,lng:lng};
			latLngArray.push(latlngObj);
		}
		function getPoints() {
			var _points = [];
			$.each(latLngArray, function(key,point){
				var position = new google.maps.LatLng(point.lat,point.lng);
				_points.push(position);
			})

			heatmap.setData(_points);
			heatmap.setMap(map);
			// heatmap.setRadius(radius:50);
		}
		console.log("LATLNGArray",latLngArray);
		var myLatlng = new google.maps.LatLng(phoneArray[0].sensor_infos[midPoint].lat, phoneArray[0].sensor_infos[midPoint].lon);
		console.log("My latlng is ",myLatlng);
		// map options,
		function initMap(){
			$("#chartContainer").height(500);
			var myOptions = {
		  		zoom: 16,
		  		center: myLatlng,
		  		mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			// standard map
			map = new google.maps.Map(document.getElementById("chartContainer"), myOptions);
			// heatmap layer
			heatmap = new google.maps.visualization.HeatmapLayer();
			getPoints();
		}
		initMap();
		
	},
	'generateGPSSpeedGraph'() {
		var phoneArray = PhoneData.find().fetch();
		var count = phoneArray[0].sensor_infos.length;
		var xArray = [];
		for(var i=0; i<count; i++) {
			var xObj = {x: phoneArray[0].sensor_infos[i].t*1000, y: phoneArray[0].sensor_infos[i].gps_s};
			xArray.push(xObj);
		}
		var newXArray = [];
		console.log("")
		if(Session.get("function")!="") {
			console.log("Derived function if statement evaluated as true");
			console.log("Derived function is:",Session.get("function"));
			var derivedFunction = Session.get("function");
			var sampleRate = Session.get("sampleRate");
			newXArray = Meteor.call(derivedFunction, xArray, sampleRate);
		}
		else {
			newXArray = xArray;
		}
		var max = Math.max.apply(Math,newXArray);
		var chart = new CanvasJS.Chart("chartContainer",
		{
		 animationEnabled: true,
		 title:{
		   text: "GPS Speed Data"
		 },
		 data: [
		 {
		 	name: "Labels",
		   	type: "area",
		   	color: "blue",
		   	xValueType: "dateTime",
		   	intervalType: "second",
		   	dataPoints: [{x:1421125364455.36987305,y:max, label: "Sitting"},
		   	{x:1421125521759.7902832,y:max, label: "Standing"}
		   	]
		   },
		   {
		 	name: "Labels",
		   	type: "area",
		   	color: "orange",
		   	xValueType: "dateTime",
		   	intervalType: "second",
		   	dataPoints: [{x:1421125521759.7902832,y:max, label: "Standing"},
		   		{x:1421125757563.48022461,y:max, label: "Running"}
		   	]
		   },
		   {
		 	name: "Labels",
		   	type: "area",
		   	color: "red",
		   	xValueType: "dateTime",
		   	intervalType: "second",
		   	dataPoints: [{x:1421125757563.48022461,y:max, label: "Running"},
		   		{x:1421126046652.87988281,y:max, label: "Running"}
		   	]
		   },
		 {
		   name:"X-axis",
		   labelFontSize: 1,
		   labelMaxWidth: 4,
		   type: "line",
		   xValueType: "dateTime",
		   intervalType: "second", 
		   showInLegend: true,
		   dataPoints: newXArray
		   }
		 ],
		 legend: {
		   cursor: "pointer",
		   itemclick: function (e) {
		     if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		       e.dataSeries.visible = false;
		     } else {
		       e.dataSeries.visible = true;
		   }
		   chart.render();
		   }
		 }
	});
	chart.render();
	}
	});

if(Meteor.isServer) {
	
	Meteor.publish('PhoneData', function(){
		return PhoneData.find({});
	});


	Router.map( function () {
  		this.route('codeEditor',{
    		waitOn: function(){
        		return [IRLibLoader.load('http://canvasjs.com/assets/script/canvasjs.min.js')]
    		}
  		});
	});
	
}

if(Meteor.isClient) {
   	Meteor.subscribe("PhoneData");
   	var myKey = 'AIzaSyCCHhbbPnayz5tlbd_4PaQsnrXqKhA2sYI';
	Meteor.startup(function(){
		GoogleMaps.load({
			key: myKey,
			libraries: 'visualization'
		});
		Session.set('data_loaded', false);
	});

	Template.map.helpers({
  		issueMapOptions: function() {
    	// Make sure the maps API has loaded
    		if (GoogleMaps.loaded()) {
      			// Map initialization options
		      return {
		        zoom: 14,
		        center: new google.maps.LatLng(-37.812566, 144.964925),
		        mapTypeId: google.maps.MapTypeId.ROADMAP,
		        mapTypeControl: false,
		        panControl: false,
		        streetViewControl: false
		      };
    		}
  		}
	});

	Template.map.onCreated(function() {
		// We can use the `ready` callback to interact with the map API once the map is ready.
		GoogleMaps.ready('issueMap', function(issueMap) {
			console.log("issuemap section entered");
			var issueData = [
			  new google.maps.LatLng(-37.812566, 144.964925),
			];       
			var issueArray = new google.maps.MVCArray(issueData);
			var heatMapLayer = new google.maps.visualization.HeatmapLayer({
				data: issueArray,
				radius: 50
			});
			heatMapLayer.setMap(issueMap.instance);
		});
	});

	Template.querySelect.events({
		'submit form':function(event){
			event.preventDefault();
			var parameter = event.target.parameters.value;
			var sampleRateString = event.target.samples.value;
			var sampleRate = parseInt(sampleRateString);
			sampleRate = sampleRate*5;
			var derivedFunction = event.target.functions.value;
			Session.set("sampleRate", sampleRate);
			Session.set("selectedParameter", parameter);
			Session.set("function",derivedFunction);
			if(parameter=="Accelerometer") {
				Meteor.call('generateAccelerometerGraph');
			}
			else if(parameter=="Magnetometer") {
				Meteor.call('generateMagnetometerGraph');
			}
			else if(parameter=="Motion Gravity Detector") {
				Meteor.call('generateMotionGravityDetectionGraph');
			}
			else if(parameter=="Motion User Acceleration") {
				Meteor.call('generateMotionUserAccelerationGraph');
			}
			else if(parameter=="Motion Attitude") {
				Meteor.call('generateMotionAttitudeGraph');
			}
			else if(parameter=="Gyroscope Rotation Rate") {
				Meteor.call('generateGyroscopeRotationGraph');
			}
			else if(parameter=="Motion Rotation Rate") {
				Meteor.call('generateMotionRotationGraph');
			}
			else if(parameter=="Magnetic Heading") {
				Meteor.call('generateMotionRotationGraph');
			}
			else if(parameter=="Plotly Tester") {
				Meteor.call('generateTester');
			}
			else if(parameter=="Calibrated Magnetic Field") {
				Meteor.call('generateCalibratedMagneticFieldGraph');
			}
			else if(parameter=="Latitude/Longitude") {
				Meteor.call('generateHeatMap');
			}
			else if(parameter=="GPS Speed") {
				Meteor.call('generateGPSSpeedGraph');
			}
		}
	});


	


	Template.dataItem.helpers({
		'getAccelerometerValues': function()  {	
			var phoneArray = PhoneData.find().fetch();
			var count = phoneArray[0].sensor_infos.length;
			console.log("count is ",count);
			var accelerometerArray = [count];
			var i = 0;
			for(i=0; i<count; i++) {
				console.log("accelerometerArray for loop entered");
				accelerometerArray[i] = phoneArray[0].sensor_infos[i].aa_x;
				console.log(accelerometerArray[i]);
			}
			return accelerometerArray;
		}
	});
}